<?php

namespace Drupal\ckeditor_spotify\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Spotify" plugin.
 *
 * @CKEditorPlugin(
 *   id = "spotify",
 *   label = @Translation("Spotify"),
 * )
 */
class SpotifyPlugin extends CKEditorPluginBase implements CKEditorPluginCssInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    return [
      drupal_get_path('module', 'ckeditor_spotify') . '/js/plugins/spotify/styles/spotify.css',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Spotify' => [
        'label' => t('Spotify'),
        'image' => drupal_get_path('module', 'ckeditor_spotify') . '/js/plugins/spotify/icons/spotify.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_spotify') . '/js/plugins/spotify/plugin.js';
  }

}
